# Awesome Solarus [![Awesome](https://cdn.rawgit.com/sindresorhus/awesome/d7305f38d29fed78fa85652e3a63e154dd8e8829/media/badge.svg)](https://github.com/sindresorhus/awesome)

[<img src="images/solarus_logo.svg" align="right" width="200">](https://www.qt.io)

> A curated list of awesome tools, libraries, and resources for the Solarus game engine.

[Solarus](https://www.solarus-games.org) is a free and open-source 2D game engine made in C++. It is multiplatform and has a Lua API, which you make your games with.

This list is inspired by all the [awesome lists](https://github.com/sindresorhus/awesome). Contributions are welcome!

## Contents

- [Official Resources](#official-resources)
- [Community](#community)
- [Quests](#quests)
- [Quest Development Resources](#quest-development-resources)
- [Other Relevant Awesome Lists](#other-relevant-awesome-lists)
- [License](#license)

## Official Resources

- [Official Website](https://www.solarus-games.org) - The official website for the Solarus engine, with downloads, news, games and more.
- [Source Code](https://gitlab.com/solarus-games) - Browse the various repositories that comprise the Solarus project.
- [Documentation](https://www.solarus-games.org/doc/latest/) - Official Solarus Quest Maker documentation for those who want to make games with Solarus.
- [Tutorial]() - Official tutorial, work in progress.
- [Video Tutorial]() - Solarus creator's video tutorial. NB: some made with an old version of Solarus, but still relevant.

## Community

- [Discord](https://discord.gg/yYHjJHt) - Official Discord server for asking for help or contributing to the engine development.
- [Forum](http://forum.solarus-games.org/) - Official forum, to ask for help, present your project and talk with the community.

## Quests

A list of complete games made with Solarus. Coming soon.

## Quest Development Resources

A list of resources (scripts, tilesets, sprites, sounds, etc.) for your project. Coming soon.

## Other Relevant Awesome Lists

- [Awesome Lua](https://github.com/LewisJEllis/awesome-lua) - You can use any Lua library to expand Solarus' own Lua API.
- [Awesome C++](https://github.com/fffaraz/awesome-cpp) - If you want to contribute to the engine development, this might interest you.
- [Awesome Qt](https://github.com/JesseTG/awesome-qt) - The Solarus Quest Editor and Solarus Launcher apps are made with the Qt Framework.
- [Awesome Game Development](https://github.com/ellisonleao/magictools) - Lots of tools to help you developing your Solarus quest.

## License

[![CC0](https://mirrors.creativecommons.org/presskit/buttons/88x31/svg/cc-zero.svg)](https://creativecommons.org/publicdomain/zero/1.0)

This list is in pubic domain.
